var tabPageVisibilityManager = (function () {
    var hiddenProperty = ["hidden", "webkitHidden", "mozHidden", "msHidden"];
    var len = hiddenProperty.length;
    var propertyKey = "";
    for (var i = 0; i < len; i++) {
        if (hiddenProperty[i] in document) {
            propertyKey = hiddenProperty[i]; //获取具体的hidden属性名
            break;
        }
    }
    var eventKey = "";
    if (propertyKey && propertyKey != "") {
        eventKey = propertyKey.replace(/hidden/i, "visibilitychange");
    }
    //获取具体事件名eventKey
    return function (pause_callback, resume_callback) {
        document.addEventListener(eventKey, function () {
            if (!document[propertyKey]) {
                if (resume_callback != null) {
                    resume_callback();
                }
            } else {
                if (pause_callback != null) {
                    pause_callback();
                }
            }
        });
    }
})();

//使用
tabPageVisibilityManager(function () {
    console.log("pause"); //暂停
}, function () {
    console.log("resume"); //恢复
});