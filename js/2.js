Function.prototype.bindX=function(thisArg,...args1){
    var that=this
    return function(...args2){
       return  that.apply(thisArg,[...args1,...args2])
    }
}

function add(num1, num2) {
    return this.value + num1 + num2;
}

var data = {
    value: 1
};

var addEx = add.bindX(data, 2);


addEx(3);
console.log(addEx(3));
