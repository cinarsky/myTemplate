'use strict';

const Controller = require('egg').Controller;

class HomeController extends Controller {
  async index(ctx) {
    // const { ctx } = this;
    console.log(ctx);

    ctx.body = 'hi, eggs';
  }
  async test() {
    const { ctx } = this;
    ctx.body = '测试接口';
  }

}

module.exports = HomeController;
