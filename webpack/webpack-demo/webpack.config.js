// let path = require('path')
// const HtmlWebpackPlugin = require('html-webpack-plugin');
// const {CleanWebpackPlugin} = require('clean-webpack-plugin');
// const webpack = require('webpack');

// module.exports = {
//     mode: 'development',
//     entry: {
//         app: './src/index.js',
//         // print: './src/print.js'
//     },
//     devtool: 'inline-source-map',
//     devServer: {
//               contentBase: './dist',
//               hot:true,
//             },
//     plugins: [
//         new CleanWebpackPlugin(),
//         new HtmlWebpackPlugin({
//             title: 'Output Management'
//         }),
//         new webpack.NamedModulesPlugin(),
//         new webpack.HotModuleReplacementPlugin()
//     ],
//     output: {
//         filename: '[name].bundle.js',
//         path: path.resolve('dist'),
//     },
//     // mode: "production",
//     module: {
//         rules: [
//             {
//                 test: /\.css$/,
//                 use: [
//                     'style-loader',
//                     'css-loader'
//                 ]
//             },
//             {
//                 test: /\.(png|svg|jpg|gif)$/,
//                 use: [
//                     'file-loader'
//                 ]
//             }
//         ]
//     }
// }

// const path = require('path');
//   const webpack = require('webpack');
//   const HTMLWebpackPlugin = require('html-webpack-plugin');

//   module.exports = {
//     entry: {
//       index: './src/index.js',
//       another: './src/another-module.js'
//     },
//     plugins: [
//       new HTMLWebpackPlugin({
//         title: 'Code Splitting'

//       }),
//     //   new webpack.optimize.CommonsChunkPlugin({
//     //     name: 'common' // 指定公共 bundle 的名称。
//     //   })
//     ],
//     output: {
//       filename: '[name].bundle.js',
//       path: path.resolve(__dirname, 'dist')
//     },
//     //替换 new webpack.optimize.CommonsChunkPlugin
//     optimization: {
//         splitChunks: {
//             cacheGroups: {
//                 commons: {
//                     name: "commons",
//                     chunks: "initial",
//                     minChunks: 2
//                 }
//             }
//         }
//     },

//   };


const path = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');
module.exports = {
    entry: {
        index: './src/index.js'

    },
    plugins: [
        new HTMLWebpackPlugin({
            title: 'Caching'

        })

    ],
    output: {
        filename: '[name].[chunkhash].js',
        // chunkFilename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist')
    }
};