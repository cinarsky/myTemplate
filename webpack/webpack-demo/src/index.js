// import _ from 'lodash';
// import { cube } from './math.js';
//  import printMe from './print.js';
// import './style.css'
//   function component() {
//   //   var element = document.createElement('div');
//   //  var btn = document.createElement('button');
//   var element = document.createElement('pre');
//     // element.innerHTML = _.join(['Hello', 'webpack'], ' ');
//     element.innerHTML = [
//             'Hello webpack!',
//             '5 cubed is equal to ' + cube(5)
//           ].join('\n\n');
//   //  btn.innerHTML = 'Click me and check the console!';
//   //  btn.onclick = printMe;

//   //  element.appendChild(btn);

//     return element;
//   }

//   document.body.appendChild(component());

//   if (module.hot) {
//         module.hot.accept('./print.js', function() {
//           console.log('Accepting the updated printMe module!');
//           printMe();
//         })
//       }

//    - import _ from 'lodash';
//    -
//    - function component() {
//   function getComponent() {
//    -   var element = document.createElement('div');
//    -
//    -   // Lodash, now imported by this script
//    -   element.innerHTML = _.join(['Hello', 'webpack'], ' ');
//     return import(/* webpackChunkName: "lodash" */ 'lodash').then(_ => {
//       var element = document.createElement('div');
 
//       element.innerHTML = _.join(['Hello', 'webpack'], ' ');
 
//       return element;
 
//     }).catch(error => 'An error occurred while loading the component');
//   }
//    - document.body.appendChild(component());
//   getComponent().then(component => {
//     document.body.appendChild(component);
//   })

  import _ from 'lodash';
 
// - async function getComponent() {
  function component() {
    var element = document.createElement('div');
// -   const _ = await import(/* webpackChunkName: "lodash" */ 'lodash');
    var button = document.createElement('button');
    var br = document.createElement('br');

    button.innerHTML = 'Click me and look at the console!';
    element.innerHTML = _.join(['Hello', 'webpack'], ' ');
    element.appendChild(br);
    element.appendChild(button);
 
    // Note that because a network request is involved, some indication
//     // of loading would need to be shown in a production-level site/app.
    button.onclick = e => import(/* webpackChunkName: "print" */ './print').then(module => {
      var print = module.default;
 
      print();
    });

    return element;
  }

// - getComponent().then(component => {
// -   document.body.appendChild(component);
// - });
  document.body.appendChild(component());