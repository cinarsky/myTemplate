// let path = require('path')
// const HtmlWebpackPlugin = require('html-webpack-plugin');
// const {CleanWebpackPlugin} = require('clean-webpack-plugin');
// const webpack = require('webpack');
// const WorkboxPlugin = require('workbox-webpack-plugin');

// module.exports = {
//     mode: 'development',
//     entry: {
//         app: './src/index.js',
//         // print: './src/print.js'
//       },
//     devtool: 'inline-source-map',
//     devServer: {
//               contentBase: './dist',
//               hot:true,
//             },
//     plugins: [
//         new CleanWebpackPlugin(),
//         new HtmlWebpackPlugin({
//             title: 'Output Management'
//         }),
//         // new webpack.NamedModulesPlugin(),
//         // new webpack.HotModuleReplacementPlugin(),
//         new WorkboxPlugin.GenerateSW({
//                   // 这些选项帮助 ServiceWorkers 快速启用
//                   // 不允许遗留任何“旧的” ServiceWorkers
//                   clientsClaim: true,
//                   skipWaiting: true
//                 })
//     ],
//     output: {
//         filename: '[name].bundle.js',
//         path: path.resolve(__dirname, 'dist')
//       },
//     // mode: "production",
//     module: {
//         rules: [
//             {
//                 test: /\.css$/,
//                 use: [
//                     'style-loader',
//                     'css-loader'
//                 ]
//             },
//             {
//                 test: /\.(png|svg|jpg|gif)$/,
//                 use: [
//                     'file-loader'
//                 ]
//             }
//         ]
//     }
// }

const path = require('path');

module.exports = {
  entry: './src/index.ts',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/
      }
    ]
  },
  devtool: 'inline-source-map',
  resolve: {
    extensions: [ '.tsx', '.ts', '.js' ]
  },
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  }
};

